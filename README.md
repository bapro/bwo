# Ultra-tests

## Tests
Please use C++ to solve the following two questions
1. Use bit operator to calculate the sum of two integers, check overflow with bit operator too.
Signature of the interface : int add(int a, int b);

2. Given an integer (T) and an integer set (I), find a subset (M) that the sum (S) of M's elements
is the largest among all subsets of I. S <= T. Signature of the interface : `void find_largest_sum(uint32_t T, const std::vector<uint32_t> &I, vector<uint32_t>& M, uint32_t &S);`
example
T: 11
I: 1 , 2, 3, 4, 5, 6,7
possible answers
M: 5,6
S:11
M: 4, 7
S: 11
You only need to find one answer if there are multiple subsets meet the requirement. Please
don’t use brute force way to solve this question, try dynamic programming if possible. Don’t
assume I is a small vector, it can contain like 10,000 numbers.


## Solutions

If you want to run/compile the tests you are going to need [catch2](https://github.com/catchorg/Catch2) (test framework) and run the cmake command with the following argument: 
`-DCATCH2_INCLUDE_PATH=/your/path/catch2`.
Tested on osx 10.15.6 with clang 11.0.3

1. Implementation can be found in `source/bwo.h` and  `source/bwo.h`, for the tests : `tests/BWOTests.cpp`
2. Implementation of the algorithm can be found: `source/subset.h` and `source/subset.cpp`, for the tests : `tests/SubsetTests.cpp`