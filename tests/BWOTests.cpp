#define CATCH_CONFIG_MAIN
#include "../source/bwo.h"
#include <catch2/catch.hpp>

TEST_CASE("Add two positive int", "[BWO]")
{
    constexpr int lhs = 3;
    constexpr int rhs = 5;
    REQUIRE(bwo::add(lhs, rhs) == (lhs + rhs));
}

TEST_CASE("Add one positive one negative int", "[BWO]")
{
    constexpr int lhs = -3;
    constexpr int rhs = 5;
    REQUIRE(bwo::add(lhs, rhs) == (lhs + rhs));
}

TEST_CASE("Add two negative int", "[BWO]")
{
    constexpr int lhs = -3;
    constexpr int rhs = -5;
    REQUIRE(bwo::add(lhs, rhs) == (lhs + rhs));
}

TEST_CASE("Check int positive overflow", "[BWO]")
{
    constexpr int lhs = INT_MAX;
    constexpr int rhs = 1;
    bool exceptionRaised = false;

    try {
        bwo::add(lhs, rhs);
    } catch (std::exception& e) {
        exceptionRaised = true;
    }

    REQUIRE(exceptionRaised == true);
}

TEST_CASE("Check int negative overflow", "[BWO]")
{
    constexpr int lhs = INT_MIN;
    constexpr int rhs = -1;
    bool exceptionRaised = false;

    try {
        bwo::add(lhs, rhs);
    } catch (std::exception& e) {
        exceptionRaised = true;
    }

    REQUIRE(exceptionRaised == true);
}

TEST_CASE("Check edge positive addition", "[BWO]")
{
    constexpr int lhs = INT_MAX - 1;
    constexpr int rhs = 1;
    bool exceptionRaised = false;

    try {
        REQUIRE(bwo::add(lhs, rhs) == (lhs + rhs));
    } catch (std::exception& e) {
        exceptionRaised = true;
    }

    REQUIRE(exceptionRaised == false);
}

TEST_CASE("Check edge negative addition", "[BWO]")
{
    constexpr int lhs = INT_MIN + 1;
    constexpr int rhs = 1;
    bool exceptionRaised = false;

    try {
        REQUIRE(bwo::add(lhs, rhs) == (lhs + rhs));
    } catch (std::exception& e) {
        exceptionRaised = true;
    }

    REQUIRE(exceptionRaised == false);
}
