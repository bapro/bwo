#include "../source/subset.h"
#include <catch2/catch.hpp>
#include <iostream>

namespace {
//Convenience function for debugging
void displayResult(const std::vector<uint32_t>& subset, uint32_t sum)
{
    if (!subset.empty()) {
        std::cout << "Found result with sum: " << sum << std::endl;
        std::cout << "[";
        for (auto value : subset) {
            std::cout << value << ", ";
        }
        std::cout << "]" << std::endl;

    } else {
        std::cout << "No result found: " << sum << std::endl;
    }
}
}

TEST_CASE("Subset tests sample", "[SBS]")
{
    std::vector<uint32_t> integerSet { 1, 2, 3, 4, 5, 6, 7 };
    uint32_t maxSum = 11;

    uint32_t sum = 0;
    std::vector<uint32_t> subset;
    sbs::find_largest_sum(maxSum, integerSet, subset, sum);

    displayResult(subset, sum);
    REQUIRE(subset.size() == 2);
    REQUIRE(sum == maxSum);
}

TEST_CASE("Subset tests not found", "[SBS]")
{
    std::vector<uint32_t> integerSet { 6, 7, 8, 9, 10 };
    uint32_t maxSum = 4;

    uint32_t sum = 0;
    std::vector<uint32_t> subset;
    sbs::find_largest_sum(maxSum, integerSet, subset, sum);

    displayResult(subset, sum);
    REQUIRE(sum == 0);
    REQUIRE(subset.empty());
}

TEST_CASE("Subset tests find closest", "[SBS]")
{
    std::vector<uint32_t> integerSet { 5, 10, 15, 20 };
    uint32_t maxSum = 14;

    uint32_t sum = 0;
    std::vector<uint32_t> subset;
    sbs::find_largest_sum(maxSum, integerSet, subset, sum);

    displayResult(subset, sum);
    REQUIRE(!subset.empty());
    REQUIRE(sum == 10);
}

TEST_CASE("Subset tests empty dataset", "[SBS]")
{
    std::vector<uint32_t> integerSet { };
    uint32_t maxSum = 14;

    uint32_t sum = 0;
    std::vector<uint32_t> subset;
    sbs::find_largest_sum(maxSum, integerSet, subset, sum);

    displayResult(subset, sum);
    REQUIRE(subset.empty());
    REQUIRE(sum == 0);
}