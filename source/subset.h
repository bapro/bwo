#pragma once

#include <cstdint>
#include <vector>

namespace sbs {
/**
 * Find a subset that give the largest sum inferior or equal of the maxSum
 * parameter between the input values set.
 * @param[in] maxSum : maximum value for the sum
 * @param[in] inputValues : dataset where to found the subset
 * @param[out] subset : subset found that give the largest sum, can be empty if
 * no solution found
 * @param[out] sum : the sum value of the subset found, can be equal to 0 if no
 * subset found
 */
void find_largest_sum(
    uint32_t maxSum,
    const std::vector<uint32_t>& inputValues,
    std::vector<uint32_t>& subset,
    uint32_t& sum);
}
