
namespace bwo {

/**
 * Add two integer number with bit operations. Throw the exception:
 * std::overflow_error if the addition overflow the int capacity.
 * @param lhs : left hand side value
 * @param rhs : right hand side value
 * @return sum of the two provided int.
 */
int add(int lhs, int rhs);
}