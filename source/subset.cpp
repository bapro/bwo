#include "subset.h"
#include <cstdint>
#include <vector>

// Anonymous namespace to not expose convenience functions
namespace {

// Compute the sum for the given subset
uint32_t subsetSum(const std::vector<uint32_t>& subset)
{
    int sum = 0;
    for (auto value : subset) {
        sum += value;
    }
    return sum;
}

std::pair<std::vector<uint32_t>, uint32_t> subsetRecursive(
    std::vector<uint32_t> inputValues,
    int sum,
    std::vector<uint32_t> subset)
{

    // We reach the end of the recursion and we have solution equal to the
    // wanted max
    if (sum == 0) {
        return std::make_pair(subset, subsetSum(subset));
    }
    // We reach the end of the recursion and we have solution but that is below
    // the max.
    else if (sum < 0) {
        // Removing the last element as it's outside the sum limit.
        subset.pop_back();
        return std::make_pair(subset, subsetSum(subset));
    }
    // We reach the end of the recursion and the subset can be return as no more
    // solution is to explore
    else if (inputValues.empty()) {
        return std::make_pair(subset, subsetSum(subset));
    }

    // Take the next value to test from the input values
    auto lastValue = inputValues.back();
    inputValues.pop_back();

    // Exploring all possibilities: with and without the current value (Knapsack
    // problem)

    // Without the last value
    auto resultExploreWithout = subsetRecursive(inputValues, sum, subset);

    // With the last value
    subset.emplace_back(lastValue);
    auto resultExploreWith = subsetRecursive(
        inputValues, sum - lastValue, subset);

    //Test wich path is the best according to the following criteria:
    // 1. return the subset with the bigest value (closer to the wanted max)
    // 2. if they are equal: return the subset that have the less elements
    if (resultExploreWithout.second > resultExploreWith.second) {
        return resultExploreWithout;
    } else if (resultExploreWithout.second < resultExploreWith.second) {
        return resultExploreWith;
    } else {
        return resultExploreWithout.first.size()
                < resultExploreWith.first.size()
            ? resultExploreWithout
            : resultExploreWith;
    }
}

}

void sbs::find_largest_sum(
    uint32_t maxSum,
    const std::vector<uint32_t>& inputValues,
    std::vector<uint32_t>& subset,
    uint32_t& sum)
{
    // Sanitizing the input/output
    subset.clear();
    sum = 0;

    auto [subsetResult, sumResult] = subsetRecursive(inputValues, maxSum, {});
    subset.swap(subsetResult);
    sum = sumResult;
}