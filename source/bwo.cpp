#include "bwo.h"
#include <iostream>

// Anonymous namespace to not expose convenience functions
namespace {
int signBit(const int& value)
{
    // Determine where the sign bit is depending on size of int (can change
    // depending on the compiler/platform)
    constexpr int signShift = (sizeof(int) * 8) - 1;
    return ((unsigned int)value) >> signShift;
}
}

namespace bwo {

int add(int lhs, int rhs)
{
    // Store the sign bit of the initial lhs value
    int signBitLhs = signBit(lhs);
    // Boolean to indicate if an overflow is possible or not:
    // true if adding two number with the same sign
    bool testOverflow = signBitLhs == signBit(rhs);

    unsigned int carry = 0;

    while (rhs != 0) {
        // compute the carry that is gonna result (and operation and left shift
        // to increase it)
        carry = (lhs & rhs) << 1;

        // XOR operation on the number
        lhs ^= rhs;
        // Use the carry on the next round
        rhs = carry;
    }

    // If an overflow was possible, check if it happened by comparing the sign
    // before the addition and the result. If overflow occured, trigger an
    // exception
    if (testOverflow && (signBit(lhs) != signBitLhs)) {
        throw std::overflow_error("addition overflowed");
    }

    return lhs;
}

}